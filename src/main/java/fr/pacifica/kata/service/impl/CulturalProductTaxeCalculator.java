package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.TaxeRate.CULTURAL_PRODUCT_TAXE;

import java.math.BigDecimal;

import fr.pacifica.kata.service.TaxeCalculator;

public class CulturalProductTaxeCalculator implements TaxeCalculator {

	@Override
	public BigDecimal getProductTaxeRate() {
		return CULTURAL_PRODUCT_TAXE.getValue();

	}

}
