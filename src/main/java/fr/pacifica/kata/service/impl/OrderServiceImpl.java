package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.ProductFamily.BASIC_NECESSITIES_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.CULTURAL_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.OTHER_PRODUCT;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.model.Product;
import fr.pacifica.kata.model.ProductFamily;
import fr.pacifica.kata.service.OrderService;

public class OrderServiceImpl implements OrderService {

	private static final List<String> BASIC_NECESSITY_PRODUCT_KEY_WORDS = Arrays.asList("chocolat", "lait", "pâte", "patate",
			"ognion", "pilule", "seringue", "médicament", "pensement");
	private static final List<String> CULTURAL_PRODUCT_KEY_WORDS = Arrays.asList("livres", "encyclopédie");
	private static final List<String> IMPORTED_PRODUCT_KEY_WORDS = Arrays.asList("importé", "importation");

	public List<Order> generateOrdersListFromDescriptions(String description) {

		return  Stream.of(description.split("\n")).map(this::generateOrderFromDescription).collect(Collectors.toList());
	}

	private Order generateOrderFromDescription(String description) {

		boolean isImported = checkIfImported(description);
		ProductFamily productType = getProductType(description);

		String[] words = description.split(" ");
		BigDecimal unitaryPrice = new BigDecimal(words[words.length - 1].replace("€", "").trim());

		String label = generateLabel(words);

		Product product = new Product(label, unitaryPrice, productType, isImported);

		int quantity = Integer.parseInt(words[1]);

		return new Order(product, quantity);
	}

	private String generateLabel(String[] words) {
		StringBuilder labelBuilder = new StringBuilder();

		for (int i = 2; i < words.length - 2; i++) {
			labelBuilder.append(words[i].trim());
			if (i != words.length - 3) {
				labelBuilder.append(" ");
			}
		}

		return labelBuilder.toString();
	}

	private ProductFamily getProductType(String describtion) {

		if (checkIfBasicNecessityProduct(describtion)) {
			return BASIC_NECESSITIES_PRODUCT;
		}

		if (checkIfCulturalProduct(describtion)) {
			return CULTURAL_PRODUCT;
		}

		return OTHER_PRODUCT;

	}

	private boolean checkIfImported(String description) {
		return IMPORTED_PRODUCT_KEY_WORDS.stream().anyMatch(description::contains);
	}

	private boolean checkIfBasicNecessityProduct(String description) {
		return BASIC_NECESSITY_PRODUCT_KEY_WORDS.stream().anyMatch(description::contains);
	}

	private boolean checkIfCulturalProduct(String description) {
		return CULTURAL_PRODUCT_KEY_WORDS.stream().anyMatch(description::contains);
	}

}
