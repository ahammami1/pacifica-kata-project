package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.TaxeRate.OTHER_PRODUCT_TAXE;

import java.math.BigDecimal;

import fr.pacifica.kata.service.TaxeCalculator;

public class OtherProductTaxeCalculator implements TaxeCalculator {

	@Override
	public BigDecimal getProductTaxeRate() {
		return OTHER_PRODUCT_TAXE.getValue();

	}

}
