package fr.pacifica.kata.service.impl;

import java.math.BigDecimal;
import java.util.List;

import fr.pacifica.kata.model.Bill;
import fr.pacifica.kata.model.BillDetail;
import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.service.BillDetailService;
import fr.pacifica.kata.service.BillService;

public class BillServicesImpl implements BillService {

	private BillDetailService billDetailService;

	public BillServicesImpl(BillDetailService billDetailService) {
		this.billDetailService = billDetailService;
	}

	@Override
	public Bill generateBill(List<Order> orders) {

		List<BillDetail> details = billDetailService.getBillDetailsFromOrderList(orders);

		BigDecimal totalPriceWithoutTaxes = getTotalPriceWithoutTaxesFromDetails(details);
		BigDecimal totalPriceWithTaxes = getTotalPriceWithTaxesFromDetails(details);

		BigDecimal totalTaxeAmount = totalPriceWithTaxes.subtract(totalPriceWithoutTaxes);

		return new Bill(details, totalPriceWithoutTaxes, totalTaxeAmount, totalPriceWithTaxes);
	}

	private BigDecimal getTotalPriceWithoutTaxesFromDetails(List<BillDetail> details) {

		return details.stream().map(BillDetail::getPriceWithoutTaxes).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	private BigDecimal getTotalPriceWithTaxesFromDetails(List<BillDetail> details) {

		return details.stream().map(BillDetail::getPriceWithTaxes).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

}
