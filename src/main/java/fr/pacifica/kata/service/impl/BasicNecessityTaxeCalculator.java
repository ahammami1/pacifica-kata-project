package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.TaxeRate.BASIC_NECESSITIES_PRODUCT_TAXE;

import java.math.BigDecimal;

import fr.pacifica.kata.service.TaxeCalculator;

public class BasicNecessityTaxeCalculator implements TaxeCalculator {

	@Override
	public BigDecimal getProductTaxeRate() {
		return BASIC_NECESSITIES_PRODUCT_TAXE.getValue();
	}

}
