package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.utilities.FormatNumberUtil.formatNumberFiveCentRound;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import fr.pacifica.kata.factory.TaxeCalculatorFactory;
import fr.pacifica.kata.model.BillDetail;
import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.model.Product;
import fr.pacifica.kata.service.BillDetailService;
import fr.pacifica.kata.service.TaxeCalculator;
import lombok.NonNull;

public class BillDetailServiceImpl implements BillDetailService {

	private TaxeCalculatorFactory taxeCalculatorFactory;

	public BillDetailServiceImpl(TaxeCalculatorFactory taxeCalculatorFactory) {
		this.taxeCalculatorFactory = taxeCalculatorFactory;
	}

	@Override
	public List<BillDetail> getBillDetailsFromOrderList(@NonNull List<Order> orders) {

		return orders.stream().map(this::getBillDetailFromOrder).collect(Collectors.toList());
	}

	private BillDetail getBillDetailFromOrder(Order order) {

		Product product = order.getProduct();
		BigDecimal priceWithoutTaxes = product.getUnitaryPrice().multiply(BigDecimal.valueOf(order.getQuantity()));
		TaxeCalculator taxCalculator = taxeCalculatorFactory.getTaxeCalculator(product.getProductFamily());
		BigDecimal taxeAmount = taxCalculator.calculateTaxesAmount(priceWithoutTaxes, product.getIsImported());
		BigDecimal priceWithTaxes = formatNumberFiveCentRound(priceWithoutTaxes.add(taxeAmount));

		return new BillDetail(order, taxeAmount, priceWithoutTaxes, priceWithTaxes);
	}

}
