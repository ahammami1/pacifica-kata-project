package fr.pacifica.kata.service;

import java.util.List;

import fr.pacifica.kata.model.Order;

public interface OrderService {

	List<Order> generateOrdersListFromDescriptions(String description);

}
