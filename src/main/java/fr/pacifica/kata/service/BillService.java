package fr.pacifica.kata.service;

import java.util.List;

import fr.pacifica.kata.model.Bill;
import fr.pacifica.kata.model.Order;

public interface BillService {

	Bill generateBill(List<Order> orders);

}
