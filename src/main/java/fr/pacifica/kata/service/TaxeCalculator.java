package fr.pacifica.kata.service;

import static fr.pacifica.kata.model.TaxeRate.IMPORTED_PRODUCT_TAXE;
import static fr.pacifica.kata.utilities.FormatNumberUtil.formatNumberFiveCentRound;

import java.math.BigDecimal;

public interface TaxeCalculator {

	default BigDecimal calculateTaxesAmount(BigDecimal priceWithoutTaxes, Boolean isImported) {
		BigDecimal taxeRate = getProductTaxeRate()
				.add(Boolean.TRUE.equals(isImported) ? IMPORTED_PRODUCT_TAXE.getValue() : BigDecimal.ZERO);
		return formatNumberFiveCentRound(priceWithoutTaxes.multiply(taxeRate).divide(BigDecimal.valueOf(100.0)));
	}

	BigDecimal getProductTaxeRate();

}
