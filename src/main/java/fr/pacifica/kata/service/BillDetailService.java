package fr.pacifica.kata.service;

import java.util.List;

import fr.pacifica.kata.model.BillDetail;
import fr.pacifica.kata.model.Order;

public interface BillDetailService {

	List<BillDetail> getBillDetailsFromOrderList(List<Order> orders);

}
