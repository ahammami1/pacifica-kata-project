package fr.pacifica.kata.model;

import java.math.BigDecimal;

import lombok.Getter;

@Getter
public enum TaxeRate {

	BASIC_NECESSITIES_PRODUCT_TAXE(BigDecimal.ZERO),
	CULTURAL_PRODUCT_TAXE(BigDecimal.TEN),
	OTHER_PRODUCT_TAXE(BigDecimal.valueOf(20.0)),
	IMPORTED_PRODUCT_TAXE(BigDecimal.valueOf(5.0));

	private final BigDecimal value;

	private TaxeRate(BigDecimal value) {
		this.value = value;
	}

}
