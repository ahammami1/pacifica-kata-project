package fr.pacifica.kata.model;

import java.math.BigDecimal;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class BillDetail {

	Order order;

	BigDecimal taxeAmount;

	BigDecimal priceWithoutTaxes;

	BigDecimal priceWithTaxes;

	@Override
	public int hashCode() {
		return Objects.hash(order, priceWithTaxes, priceWithoutTaxes, taxeAmount);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillDetail other = (BillDetail) obj;
		return Objects.equals(order, other.order)
				&& Objects.equals(priceWithTaxes.doubleValue(), other.priceWithTaxes.doubleValue())
				&& Objects.equals(priceWithoutTaxes.doubleValue(), other.priceWithoutTaxes.doubleValue())
				&& Objects.equals(taxeAmount.doubleValue(), other.taxeAmount.doubleValue());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("* ");
		sb.append(order.getQuantity()).append(" ").append(order.getProduct().getLabel()).append(" à ")
				.append(order.getProduct().getUnitaryPrice()).append("€ : ").append(priceWithTaxes).append("€ TTC \n");

		return sb.toString();
	}

}
