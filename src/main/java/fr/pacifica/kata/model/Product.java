package fr.pacifica.kata.model;

import java.math.BigDecimal;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class Product {

	String label;

	BigDecimal unitaryPrice;

	ProductFamily productFamily;

	Boolean isImported;

	@Override
	public int hashCode() {
		return Objects.hash(isImported, label, productFamily, unitaryPrice);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return Objects.equals(isImported, other.isImported) && Objects.equals(label, other.label)
				&& Objects.equals(productFamily, other.productFamily)
				&& Objects.equals(unitaryPrice.doubleValue(), other.unitaryPrice.doubleValue());
	}
}
