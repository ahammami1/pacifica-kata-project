package fr.pacifica.kata.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class Bill {

	List<BillDetail> billDetails;

	BigDecimal totalPriceWithoutTaxes;

	BigDecimal totalTaxeAmount;

	BigDecimal totalPriceWithTaxes;

	@Override
	public int hashCode() {
		return Objects.hash(billDetails, totalPriceWithTaxes, totalPriceWithoutTaxes, totalTaxeAmount);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bill other = (Bill) obj;
		return Objects.equals(billDetails, other.billDetails)
				&& Objects.equals(totalPriceWithTaxes.doubleValue(), other.totalPriceWithTaxes.doubleValue())
				&& Objects.equals(totalPriceWithoutTaxes.doubleValue(), other.totalPriceWithoutTaxes.doubleValue())
				&& Objects.equals(totalTaxeAmount.doubleValue(), other.totalTaxeAmount.doubleValue());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");

		billDetails.forEach(detail -> sb.append(detail.toString()));

		sb.append("\n\n").append("Montant des taxes : ").append(totalTaxeAmount).append("€").append("\n")
				.append("Total : ").append(totalPriceWithTaxes.setScale(2)).append("€");

		return sb.toString();
	}

}
