package fr.pacifica.kata.model;

public enum ProductFamily {

	BASIC_NECESSITIES_PRODUCT, CULTURAL_PRODUCT, OTHER_PRODUCT;
}
