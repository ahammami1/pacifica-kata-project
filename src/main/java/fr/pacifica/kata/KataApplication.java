package fr.pacifica.kata;

import fr.pacifica.kata.factory.impl.TaxeCalculatorFactoryImpl;
import fr.pacifica.kata.model.Bill;
import fr.pacifica.kata.service.BillService;
import fr.pacifica.kata.service.OrderService;
import fr.pacifica.kata.service.impl.BillDetailServiceImpl;
import fr.pacifica.kata.service.impl.BillServicesImpl;
import fr.pacifica.kata.service.impl.OrderServiceImpl;

public class KataApplication {

	public static void main(String[] args) {

		/**
		 * Setup Services
		 **/
		BillService billService = new BillServicesImpl(new BillDetailServiceImpl(new TaxeCalculatorFactoryImpl()));
		OrderService orderService = new OrderServiceImpl();

		/**
		 * input definition
		 */
		String input1 = "* 2 livres à 12.49€\r\n" + "* 1 CD musical à 14.99€\r\n" + "* 3 barres de chocolat à 0.85€";

		String input2 = "* 2 boîtes de chocolats importée à 10€\r\n" + "* 3 flacons de parfum importé à 47.50€";

		String input3 = "* 2 flacons de parfum importé à 27.99€\r\n" + "* 1 flacon de parfum à 18.99€\r\n"
				+ "* 3 boîtes de pilules contre la migraine à 9.75€\r\n" + "* 2 boîtes de chocolats importés à 11.25€";

		/**
		 * Generate Bills
		 */
		Bill firstBill = billService.generateBill(orderService.generateOrdersListFromDescriptions(input1));

		Bill secondBill = billService.generateBill(orderService.generateOrdersListFromDescriptions(input2));

		Bill thirdBill = billService.generateBill(orderService.generateOrdersListFromDescriptions(input3));

		/**
		 * display Bills
		 */
		System.out.println();
		System.out.println("#### Output 1");
		System.out.println();

		System.out.println(firstBill);
		System.out.println();

		System.out.println("#### Output 2");
		System.out.println();
		System.out.println(secondBill);

		System.out.println();

		System.out.println("#### Output 3");
		System.out.println();
		System.out.println(thirdBill);

	}

}
