package fr.pacifica.kata.utilities;

import java.math.BigDecimal;
import static java.math.BigDecimal.*;

import java.math.RoundingMode;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FormatNumberUtil {

	private final BigDecimal HALF_VALUE = BigDecimal.valueOf(0.5);

	public static BigDecimal formatNumberFiveCentRound(BigDecimal numberToFormat) {

		BigDecimal bigDecimal = numberToFormat.multiply(TEN);
		BigDecimal intValue = bigDecimal.setScale(0, RoundingMode.DOWN);

		BigDecimal decimalPart = bigDecimal.subtract(intValue);

		if (decimalPart.compareTo(ZERO) == 0) {
			return intValue.divide(TEN);
		}

		return (intValue.add(decimalPart.compareTo(HALF_VALUE) > 0 ? ONE : HALF_VALUE)).divide(TEN);
	}

}
