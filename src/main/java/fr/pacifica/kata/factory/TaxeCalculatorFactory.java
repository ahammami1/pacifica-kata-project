package fr.pacifica.kata.factory;

import fr.pacifica.kata.model.ProductFamily;
import fr.pacifica.kata.service.TaxeCalculator;

public interface TaxeCalculatorFactory {
	TaxeCalculator getTaxeCalculator(ProductFamily productFamily);

}
