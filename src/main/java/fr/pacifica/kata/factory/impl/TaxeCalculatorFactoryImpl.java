package fr.pacifica.kata.factory.impl;

import fr.pacifica.kata.exceptions.TaxeFactoryException;
import fr.pacifica.kata.factory.TaxeCalculatorFactory;
import fr.pacifica.kata.model.ProductFamily;
import fr.pacifica.kata.service.TaxeCalculator;
import fr.pacifica.kata.service.impl.BasicNecessityTaxeCalculator;
import fr.pacifica.kata.service.impl.CulturalProductTaxeCalculator;
import fr.pacifica.kata.service.impl.OtherProductTaxeCalculator;
import lombok.NonNull;

public class TaxeCalculatorFactoryImpl implements TaxeCalculatorFactory {

	@Override
	public TaxeCalculator getTaxeCalculator(@NonNull ProductFamily productFamily) {

		switch (productFamily) {

		case BASIC_NECESSITIES_PRODUCT:
			return new BasicNecessityTaxeCalculator();

		case CULTURAL_PRODUCT:
			return new CulturalProductTaxeCalculator();

		case OTHER_PRODUCT:
			return new OtherProductTaxeCalculator();

		default:
			throw new TaxeFactoryException(
					String.format("Unable to build a taxeCalulculator with param %s", productFamily));

		}
	}

}
