package fr.pacifica.kata.exceptions;

public class TaxeFactoryException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TaxeFactoryException(String message) {
		super(message);
	}

}
