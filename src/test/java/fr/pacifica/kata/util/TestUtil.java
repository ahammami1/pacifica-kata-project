package fr.pacifica.kata.util;

import static fr.pacifica.kata.model.ProductFamily.BASIC_NECESSITIES_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.CULTURAL_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.OTHER_PRODUCT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.pacifica.kata.model.BillDetail;
import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.model.Product;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TestUtil {
	
	public static List<Order> getSampleListOfOrders(){
		
		Product books = new Product("livres", BigDecimal.valueOf(12.49), CULTURAL_PRODUCT, false);
		Order booksOrder = new Order(books, 2);

		Product musicCD = new Product("CD musical", BigDecimal.valueOf(14.99), OTHER_PRODUCT, false);
		Order musicCDOrder = new Order(musicCD, 1);

		Product choclate = new Product("barres de chocolat", BigDecimal.valueOf(0.85), BASIC_NECESSITIES_PRODUCT,
				false);
		Order choclateOrder = new Order(choclate, 3);

		List<Order> orders = new ArrayList<>();

		orders.add(booksOrder);
		orders.add(musicCDOrder);
		orders.add(choclateOrder);
		return orders;
	}
	
	public static List<BillDetail> getSampleListOfBillDetails(){
		
		List<Order> orders = getSampleListOfOrders();

		BillDetail expectedBookResult = new BillDetail(orders.get(0), BigDecimal.valueOf(2.5), BigDecimal.valueOf(24.98),
				BigDecimal.valueOf(27.5));
		BillDetail expectedMusicCDResult = new BillDetail(orders.get(1), BigDecimal.valueOf(3.0),
				BigDecimal.valueOf(14.99), BigDecimal.valueOf(18));
		BillDetail expectedChoclateResult = new BillDetail(orders.get(2), BigDecimal.valueOf(0.0),
				BigDecimal.valueOf(2.55), BigDecimal.valueOf(2.55));

		return Arrays.asList(expectedBookResult, expectedMusicCDResult, expectedChoclateResult);

	}

}
