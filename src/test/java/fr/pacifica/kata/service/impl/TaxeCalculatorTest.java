package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.ProductFamily.BASIC_NECESSITIES_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.CULTURAL_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.OTHER_PRODUCT;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import fr.pacifica.kata.factory.impl.TaxeCalculatorFactoryImpl;
import fr.pacifica.kata.model.ProductFamily;
import fr.pacifica.kata.service.TaxeCalculator;

class TaxeCalculatorTest {

	@ParameterizedTest
	@MethodSource("provideProductInfo")
	void calculateTaxesAmountTest(BigDecimal price, Boolean isImported, ProductFamily productType, BigDecimal expected) {

		TaxeCalculatorFactoryImpl factory = new TaxeCalculatorFactoryImpl();
		TaxeCalculator taxeCalculator = factory.getTaxeCalculator(productType);
		Assertions.assertEquals(expected, taxeCalculator.calculateTaxesAmount(price, isImported));
	}
	
	
	private static Stream<Arguments> provideProductInfo() {
	    return Stream.of(
	      Arguments.of(new BigDecimal("2.55"),false ,BASIC_NECESSITIES_PRODUCT, ZERO),
	      Arguments.of(new BigDecimal("20"),true ,BASIC_NECESSITIES_PRODUCT, ONE),
	      Arguments.of(new BigDecimal("24.98"),false ,CULTURAL_PRODUCT, new BigDecimal("2.5")),
	      Arguments.of(new BigDecimal("24.98"),true ,CULTURAL_PRODUCT, new BigDecimal("3.75")),
	      Arguments.of(new BigDecimal("24.98"),true ,OTHER_PRODUCT, new BigDecimal("6.25")),
	      Arguments.of(new BigDecimal("10"),false ,OTHER_PRODUCT, new BigDecimal("2"))

	      );
	}

}
