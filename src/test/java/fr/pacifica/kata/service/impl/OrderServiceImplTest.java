package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.util.TestUtil.getSampleListOfOrders;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.service.OrderService;

class OrderServiceImplTest {

	private OrderService orderService;

	@BeforeEach
	void setup() {
		orderService = new OrderServiceImpl();
	}

	@ParameterizedTest
	@ValueSource(strings = {
			"* 2 livres à 12.49€\r\n" + "* 1 CD musical à 14.99€\r\n" + "* 3 barres de chocolat à 0.85€" })
	void getOrderFromDescriptionTest(String input) {

		List<Order> orders = getSampleListOfOrders();

		Assertions.assertEquals(orders, orderService.generateOrdersListFromDescriptions(input));

	}

}
