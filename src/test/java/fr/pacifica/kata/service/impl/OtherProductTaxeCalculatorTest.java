package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.TaxeRate.OTHER_PRODUCT_TAXE;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.pacifica.kata.service.TaxeCalculator;

class OtherProductTaxeCalculatorTest {

	@Test
	void otherProductTaxeRateTest() {
		TaxeCalculator taxeCalculator = new OtherProductTaxeCalculator();
		Assertions.assertEquals(OTHER_PRODUCT_TAXE.getValue(), taxeCalculator.getProductTaxeRate());

	}
}
