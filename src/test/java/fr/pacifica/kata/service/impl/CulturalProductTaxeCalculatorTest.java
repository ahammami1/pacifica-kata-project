package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.TaxeRate.CULTURAL_PRODUCT_TAXE;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.pacifica.kata.service.TaxeCalculator;

class CulturalProductTaxeCalculatorTest {
	@Test
	void otherProductTaxeRateTest() {
		TaxeCalculator taxeCalculator = new CulturalProductTaxeCalculator();
		Assertions.assertEquals(CULTURAL_PRODUCT_TAXE.getValue(), taxeCalculator.getProductTaxeRate());

	}

}
