package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.util.TestUtil.getSampleListOfBillDetails;
import static fr.pacifica.kata.util.TestUtil.getSampleListOfOrders;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.pacifica.kata.factory.impl.TaxeCalculatorFactoryImpl;
import fr.pacifica.kata.model.BillDetail;
import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.service.BillDetailService;

class BillDetailServiceImplTest {

	private BillDetailService billDetailService;

	@BeforeEach
	void setup() {
		billDetailService = new BillDetailServiceImpl(new TaxeCalculatorFactoryImpl());

	}

	@Test
	void getBillDetailsFromOrdersTest() {

		List<Order> orders = getSampleListOfOrders();
		List<BillDetail> expectedBillDetailList = getSampleListOfBillDetails();
		Assertions.assertEquals(expectedBillDetailList, billDetailService.getBillDetailsFromOrderList(orders));

	}

}
