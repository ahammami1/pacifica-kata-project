package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.model.TaxeRate.BASIC_NECESSITIES_PRODUCT_TAXE;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.pacifica.kata.service.TaxeCalculator;

class BasicNecessityTaxeCalculatorTest {

	@Test
	void basicNecessityTaxeRateTest() {
		TaxeCalculator taxeCalculator = new BasicNecessityTaxeCalculator();
		Assertions.assertEquals(BASIC_NECESSITIES_PRODUCT_TAXE.getValue(), taxeCalculator.getProductTaxeRate());

	}

}
