package fr.pacifica.kata.service.impl;

import static fr.pacifica.kata.util.TestUtil.getSampleListOfBillDetails;
import static fr.pacifica.kata.util.TestUtil.getSampleListOfOrders;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.pacifica.kata.factory.impl.TaxeCalculatorFactoryImpl;
import fr.pacifica.kata.model.Bill;
import fr.pacifica.kata.model.BillDetail;
import fr.pacifica.kata.model.Order;
import fr.pacifica.kata.service.BillService;

class BillServicesImplTest {

	private BillService billService;

	@BeforeEach
	void setup() {
		billService = new BillServicesImpl(new BillDetailServiceImpl(new TaxeCalculatorFactoryImpl()));
	}

	@Test
	void getBillFromOrdersTest() {

		List<Order> orders = getSampleListOfOrders();
		List<BillDetail> expectedBillDetailList = getSampleListOfBillDetails();
		Bill expectedResult = new Bill(expectedBillDetailList, BigDecimal.valueOf(42.52), BigDecimal.valueOf(5.53),
				BigDecimal.valueOf(48.05));

		Assertions.assertEquals(expectedResult, billService.generateBill(orders));

	}

}
