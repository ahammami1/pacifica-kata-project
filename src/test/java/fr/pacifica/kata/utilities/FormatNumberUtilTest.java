package fr.pacifica.kata.utilities;

import static fr.pacifica.kata.utilities.FormatNumberUtil.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class FormatNumberUtilTest {

	@ParameterizedTest
	@CsvSource(value = { "1.00:1.00", "1.42:1.45", "1.01:1.05", "1.02:1.05", "0.99:1.00", "1.48:1.5" }, delimiter = ':')
	void formatNumberWithFiveCentTest(BigDecimal input, BigDecimal expected) {

		Assertions.assertEquals(0, expected.compareTo(formatNumberFiveCentRound(input)));
	}


}
