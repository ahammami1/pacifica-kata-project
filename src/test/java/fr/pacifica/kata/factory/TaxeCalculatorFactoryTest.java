package fr.pacifica.kata.factory;

import static fr.pacifica.kata.model.ProductFamily.BASIC_NECESSITIES_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.CULTURAL_PRODUCT;
import static fr.pacifica.kata.model.ProductFamily.OTHER_PRODUCT;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import fr.pacifica.kata.factory.impl.TaxeCalculatorFactoryImpl;
import fr.pacifica.kata.model.ProductFamily;
import fr.pacifica.kata.service.TaxeCalculator;
import fr.pacifica.kata.service.impl.BasicNecessityTaxeCalculator;
import fr.pacifica.kata.service.impl.CulturalProductTaxeCalculator;
import fr.pacifica.kata.service.impl.OtherProductTaxeCalculator;

class TaxeCalculatorFactoryTest {

	TaxeCalculatorFactory factory;

	@BeforeEach
	void setup() {
		factory = new TaxeCalculatorFactoryImpl();
	}


		
	@Test
	void getTaxeCalculatorInstanceShouldThrowErrorTest() {

		 assertThrows(NullPointerException.class, () -> factory.getTaxeCalculator(null));


	}
	
	@ParameterizedTest
	@MethodSource("provideProductFamilies")
	void taxeCaluculatorFactoryTest(ProductFamily input, Class<TaxeCalculator> expected) {
		
		TaxeCalculator taxCalculator = factory.getTaxeCalculator(input);

		Assertions.assertInstanceOf(expected, taxCalculator);
	}
	
	
	private static Stream<Arguments> provideProductFamilies() {
	    return Stream.of(
	      Arguments.of(BASIC_NECESSITIES_PRODUCT, BasicNecessityTaxeCalculator.class),
	      Arguments.of(CULTURAL_PRODUCT, CulturalProductTaxeCalculator.class),
	      Arguments.of(OTHER_PRODUCT, OtherProductTaxeCalculator.class)
	      );
	}
	   

}
